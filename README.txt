 _   _         _      _           ___                         _   
( ) ( )       ( )    (_ )        (  _`\                      ( )_ 
| `\| |   _   | |_    | |    __  | ( (_)   _    _   _   ___  | ,_)
| , ` | /'_`\ | '_`\  | |  /'__`\| |  _  /'_`\ ( ) ( )/' _ `\| |  
| |`\ |( (_) )| |_) ) | | (  ___/| (_( )( (_) )| (_) || ( ) || |_ 
(_) (_)`\___/'(_,__/'(___)`\____)(____/'`\___/'`\___/'(_) (_)`\__)


Credits
http://tpgblog.com/noblecount/

Script from kumichou fork of NobleCount by theproductguy
https://github.com/theproductguy/NobleCount/pull/1

Minimified using UglifyJS @ http://marijnhaverbeke.nl//uglifyjs

ASCII graphic by http://www.network-science.de/ascii/

See branches for code.
